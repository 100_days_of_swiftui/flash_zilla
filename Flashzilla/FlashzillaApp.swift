//
//  FlashzillaApp.swift
//  Flashzilla
//
//  Created by Hariharan S on 06/07/24.
//

import SwiftUI

@main
struct FlashzillaApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

